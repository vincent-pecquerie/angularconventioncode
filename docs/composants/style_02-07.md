## Style 02-07 : Utiliser un prefix personnalisée pour nommer les composants

* **Règles:**
    * Ajouter un prefix personnalisée pour nommer les composants. (Ex : `sii-topbar`)
    * Ce prefix peut-être organisationnelle (ex : `sii-`) ou associé à une fonctionnalité (ex : `users-login`). 
* **Objectifs:**
    * Eviter les collisions entre les différents composants interne et externes.
    * Permettre d’extraire et de réutiliser certains composants dans d’autres projets en les incluant dans une librairie de composant par exemple. 

### Exemple: 

#### A Eviter : 

```typescript
// HeroComponent is in the Tour of Heroes feature 
@Component({ 
    selector: 'hero' 
}) 
export class HeroComponent {

}
```

#### A Préférer :

```typescript
@Component({ 
    selector: 'toh-hero' 
}) 
export class HeroComponent {

}
```
