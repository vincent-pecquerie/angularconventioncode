## Style 04-05 : Ne pas se répéter.

* **Règles :**
    * Si un bloc de code est répété dans un ou plusieurs composant(s), celui-ci doit être extrait dans un nouveau composant. 

* **Objectifs :**
    * Ne pas se répeter (DRY : Don’t Repeat Yourself).
    * Augmenter la lisibilité du code en factorisant le code.

