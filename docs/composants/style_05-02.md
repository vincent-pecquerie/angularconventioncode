## Style 05-02 : Utiliser la notation kebab-case pour nommer les composants

* **Règles :** 
  * Utiliser la notation kebab case pour nommer le sélecteur des composants. 

* **Objectifs :**
    * Respecter la norme W3C sur les custom éléments (https://www.w3.org/TR/custom-elements/)
