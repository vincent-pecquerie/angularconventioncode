## Style 05-03 : Préférer utiliser les composants en tant qu’élément plutôt qu’attribut ou classe.

* **Règles :**
    * Il est préférable de ne pas utiliser les sélecteurs d’attribut ou de classe mais de privilégier les composants éléments. Cela réduit la confusion entre attribut et composants.
* **Objectifs :**
    * Augmenter la lisibilité du code.
