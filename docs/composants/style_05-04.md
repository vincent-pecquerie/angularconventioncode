## Style 05-04 : Extraire le template HTML et le CSS dans des fichiers dédiés.

* **Règles :**
    * Créer un fichier [nom-composant].component.html et [nom-composant].component.css
* **Objectifs :**
    * Augmenter la lisibilité du code en factorisant le code.
