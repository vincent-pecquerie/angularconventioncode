## Style 05-14 : Ordonnées les propriétés

* **Règles :**
    * Placer les membres publics en priorité puis les membres privés.
    * Trier ensuite les membres par ordre alphabétique.
* **Objectifs :**
    * Augmenter la lisibilité du code
