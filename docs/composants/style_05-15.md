## Style 05-15 : Déléguer les logiques métiers à des couches services

* **Règles :**
    * Limiter les actions du contrôleur au action sur la vue.
    * Déléguer tout la logique métier dans des services.
* **Objectifs :**
    * Isoler la partie métier de la partie technique.
    * Faciliter les tests unitaires sur la partie métier
    * Réduire les dépendances d’un composant.
    * Garder des composants simple, factorisable et réutilisable.
 
