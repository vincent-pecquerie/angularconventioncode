## Style 05-16 : Ne pas préfixer les propriété Output

* **Règles :**
    * Ne pas préfixer les méthodes Output.
    * A la place, il est préférable de préfixer la méthode appeler.

* **Objectifs :**
    * Rester cohérent avec le framework
    * Permettre l’utilisation d’appel via attribut.

### Exemple :

Ne pas faire :

```typescript
@Component({ 
    selector: 'toh-hero', 
    template: `<toh-hero (onSavedTheDay)="onSavedTheDay($event) "></toh-hero>` 
}) 
export class HeroComponent { 
    @Output() onSavedTheDay = new EventEmitter<boolean>(); 
}
```

Mais plutôt :

```typescript
@Component({ 
    selector: 'toh-hero', 
    template: `<toh-hero (savedTheDay)="onSavedTheDay($event)"></toh-hero>` 
}) 
export class HeroComponent { 
@Output() savedTheDay = new EventEmitter<boolean>(); 

}
```

