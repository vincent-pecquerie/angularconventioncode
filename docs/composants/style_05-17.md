## Style 05-17 : Eviter de mettre la logique de présentation dans le template

* **Règles :**
    * Eviter de placer la logique de présentation dans le template.
* **Objectifs :**
    * Ne pas séparer la logique de présentation dans plusieurs fichiers.
    * Permettre rentre la logique de présentation testables.

### Exemple
 
Ne pas faire : 

```html
Average power: {{totalPowers / heroes.length}}
```

Mais plutôt :

```html
Average power: {{avgPower}}
```

et Dans le contrôleur :

```typescript
get avgPower() { 
    return this.totalPowers / this.heroes.length; 
}
``` 

