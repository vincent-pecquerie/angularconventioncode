## Style 09-01 : Gérer le cycle de vie de son composant

* **Règle :**
    * Implémenter les interfaces de gestion du cycle de vie et prendre en compte les actions sur la mémoire et les données. 
    * Prévoir l’initialisation et la destruction des données du composant.

* **Objectifs :**
    * Limiter les effets de bords
    * Limiter les fuites mémoires

### Evenements d'un composant

#### OnChange	

Survient qu’un élément bindé est modifié par angular ou action utilisateur. 
La méthode reçoit en paramètre un objet SimpleChanges de l’état avant / après. 
Survient également une première fois avant le `ngOnInit()`.

#### OnInit	

Initialise le composant après qu’Angular ait initialisé les variables @Input.
Appelé une seul fois, après le premier `ngOnChanges()`.

#### DoCheck	
Vérifie et agit quand un changement extérieur à Angular est effectué. 
Appelé immediatement après chaque `ngOnChanges()` et `ngOnInit()`

#### AfterContentInit	
Retourne les changements extérieur à Angular lors du chargement de la page 
S’exécute après le premier `ngDoCheck()`.

#### AfterContentChecked	
Retourne les changements extérieurs à Angular 
S’exécute après chacque `ngDoCheck()`.

#### AfterViewInit	
Agit après qu’Angular ait initialisé la vue et les composants inclut dans cette vue.
S’exécute après le premier `ngAfterContentChecked()`.

#### AfterViewChecked	
Agit après qu’Angular ait vérifié la vue et les composants inclut dans cette vue.
S’exécute après chaque `ngAfterContentChecked()`.

#### OnDestroy	
S’execute juste avant qu’Angular détruise le composant. 
Doit servir à se désabonner des observables pour éviter les fuites de mémoire.

#### Références

:link: [Documentation Officielle](https://angular.io/guide/lifecycle-hooks)