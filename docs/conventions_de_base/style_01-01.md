## Style 01-01 : Un fichier de code ne devrait pas faire plus de 400 lignes

* **Règle :** Responsabilité Unique (S de SOLID), chaque fichier que ce soit un composant, un service ou une simple classe ne doit faire qu’une et une seule chose. Cela permet de garder l’application lisible et facile à maintenir.
* **Objectifs :** Avoir un code simple à lire, comprendre et maintenir. 
* **Bénéfices :**
    *	Un fichier court et concis est plus rapide à lire, maintenir et limite les risques de conflits.
    *	Un fichier court et concis limite le nombre de variables et de fonction utilisant la même portée.
    *	Le découplage en petit fichier permet l’injection de plus petite dépendances et donc la factorisation du code.
    *	Le découplage en petit fichier permet de tester plus facilement tout ou partie de l’application.
