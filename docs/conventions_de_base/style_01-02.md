## Style 01-02 : Une fonction ne devrait pas faire plus de 75 lignes

* **Règle :** Responsabilité Unique (S de Solid), chaque fonction ne devrait avoir qu’un et un seul but.
* **Objectifs :** Avoir un code simple à lire, comprendre et maintenir. 
* **Bénéfices :**
    * Une fonction courte et concise est plus rapide à lire, maintenir et limite les risques de conflits.
    * Une fonction courte et concise limite le nombre de paramètres et rend le code plus facilement testable.
    * Le découplage en petite fonction permet de découpler les fonctionnalités et de rentre le code plus facilement factorisable. 
