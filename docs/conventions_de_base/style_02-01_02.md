## Style 02-01/02 : Utiliser le nommage des fichiers recommandé.

* **Règle :** Nommer les fichiers en utilisant le pattern recommandé [feature].[type].ts
* **Paramètres :** 
    * **[feature] :** Nom de la fonctionnalité, notation kebab-case (ex : hero-list).
    * **[type] :** Type de fichier explicite regroupant la couche logique associé (Ex : component, module, service, pipes, …). 
* **Objectifs :**
	* Une convention de nommage permet de retrouver rapidement une ressource.
        * Cela permet d’identifier l’objectif du fichier sans l’ouvrir.
    * Une convention de nommage permet d’avoir de la consistance entre les développements
        * Cela fait gagner du temps aux développeurs en place (automatisme).
        * Cela fait gagner du temps aux nouveaux développeurs (consistante). 

![Symbole](img/style_02-01_02_001.png)