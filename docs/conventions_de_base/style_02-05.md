## Style 02-05 : Mettre la logique du démarrage et plateforme dans le fichier main.ts

* **Règles :**
    * Placer la logique du bootstrapping et de plateforme dans le fichier main.ts.
* **Objectifs :**
    * Avoir un point d’entrée unique.
* **Exemple :**

```typescript
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
```

