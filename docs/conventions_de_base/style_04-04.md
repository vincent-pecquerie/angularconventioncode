## Style 04-04 : Organiser les fonctionnalités par dossier

* **Règles :**
    * Découper en sous dossiers si celui-ci contient au plus de 7 fichiers.
    * Configurer l’IDE pour masque les fichiers .js et .js.map

* **Objectifs :**
    * Identifier rapidement les fonctionnalités.

![Organisation des dossiers par fonctionnalié](img/style_04-04_001.png)