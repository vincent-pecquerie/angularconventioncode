## Style 04-06 : Structure de dossier

* **Règles :**
    * Regrouper les fonctionnalités par dossier.
    * Créer un dossier shared pour les fonctions / composants transverses.
* **Objectifs :**
    * Identifier rapidement les fonctionnalités.
    * Avoir de la consistance entre les développements.


![Organisation des dossiers par fonctionnalié](img/style_04-04_001.png)