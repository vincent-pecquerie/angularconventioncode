## Utiliser des alias pour les imports réguliers

* **Règle :**
    * Mettre en place des alliasses dans l’application pour     réduire les chemins partagés.

* **Objectifs :**
    * Factoriser les imports
    * Rendre le chemin des imports plus lisible.

* **Mise en place :**

Prenons pour exemple :  

```typescript
import { LoaderService } from '../../../loader/loader.service';
```

Il faut ajout dans le fichier *tsconfig.json* les éléments suivants : 

```json
{
  "compileOnSave": false,
  "compilerOptions": {
    "paths": {
      "@app/*": ["app/*"],
    }
  }
}
```
On pourra ensuite utiliser l’alias : 

```typescript
import { LoaderService } from '@app/loader/loader.service';
```
