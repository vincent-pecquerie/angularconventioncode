## Style 02-06 : Utiliser la notation lowerCamelCase pour nommer les sélecteurs de directives

* **Règles:**
    * Utiliser la notation lowerCamelCase pour nommer les sélecteurs de directives
* **Objectif:**
    * Garder de la consistance entre le nom de la directive et l’attribut associé.
 
