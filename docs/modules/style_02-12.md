## Style 02-12 : Convention de nommage

* **Règles:**
    * Suffixer le nom de la classe avec Module.
    * Suffixer le nom du fichier avec .module.ts
    * Suffixer de RoutingModule un module de routage.
    * Suffixer le nom d’un fichier RoutingModule avec –routing.module.ts
    * Utiliser la notation UpperCamelCase.
    * Créer un dossier associé si le module inclut des éléments.

* **Objectifs :**
    * Garder de la consistance entre les différentes classes.
    * Identifier rapidement les modules.

* **Exemples:**
![Exemples](img/style_02-12_001.png)