## Style 04-07 : Dossier par fonctionnalité 

* **Règles:** 
  * Créer un dossier par fonctionnalité.
  * Créer un module associé.

* **Objectifs:**
    * Découpler le code et rendre, de ce fait, le code plus facilement testable
    * Réduire le temps de chargement en rendant le module facultatif et lazy loadable.
    * Respecter les principes de la POO en encapsulant les composants dans un conteneur qui peut être au choix publique ou privée.

