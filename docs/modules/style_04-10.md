## Style 04-10 : Module Shared pour les éléments partagés.

* **Règles:**
    * Créer un module SharedModule
    * Créer un dossier associé regroupant les éléments communs à l’application ou si nécessaire à un ensemble de fonctionnalités.
    * Importer dans ce module l’ensemble des modules nécessaire à l’ensemble de l’application tel que le CommonModule, FormsModule… 

* **Objectifs:**

    * Identifier rapidement les ressources partagées.
    * Factoriser les développements
    * Réduire les dépendances.
