## Performance : Utilisation du trackby sur un des boucles

* **Règles :**
    * Si une boucle est faite sur une collection complexe et/ou importante d’éléments, utiliser l’instruction trackBy.
* **Objectifs :**
    * Eviter à Angular de rendre à nouveau l’ensemble du DOM.
    * Définir quels éléments est modifiés ou supprimés…
* **Exemple :**

```html
<li *ngFor="let item of items; trackBy: trackByFn">
{{ item }}
</li>
```
```typescript
trackByFn(index, item) {    
   return item.id; // Element unique permettant d’identifier un élément les uns des autres.
}
```

* **Ressources :**

:link: [Article FreeCodeCamp](https://www.freecodecamp.org/news/best-practices-for-a-clean-and-performant-angular-application-288e7b39eb6f/)
:link: [Article NetBasal](https://netbasal.com/angular-2-improve-performance-with-trackby-cc147b5104e5)
:link: [Article Medium](https://medium.com/better-programming/improving-angular-ngfor-performance-through-trackby-ae4cf943b878)
