## Performance : Utiliser le lazyloading de module

* **Règles :**
    * Exporter les composants et fonctionnalités dans de modules séparé.
    * Exporter le routing dans un module séparé.
    * Utiliser le lazy loading pour charger les différentes pages de l’application.
* **Objectifs :**
    * Réduire le temps de chargement de l’application.
    * Réduire l’empreinte mémoire et réseau de l’application.
    * Rendre le code plus facilement réutilisable et testable.

Références : 

:link: [Article WishTack](https://guide-angular.wishtack.io/angular/routing/lazy-loading)
