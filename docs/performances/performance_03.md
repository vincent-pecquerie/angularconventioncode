## Performance : Supprimer les espaces dans le code minifié.

Il est possible de préciser dans Angular 5+ que l’on souhaite supprimer les espaces dans les templates lors de la minification. Cela va réduire le poids global du bundle et accélérer le temps de chargement de l’application.

Il est possible de l’activer globalement via l’instruction suivante dans le `tsconfig.json` :

```json
"angularCompilerOptions": {
    "preserveWhitespaces": false
}
```

Mais il est aussi possible de le faire par composant directement dans le décorateur : 

```typescript
@Component({
    selector: 'my-progress-bar',
    templateUrl: 'progress-bar.component.html',
    preserveWhitespaces: false
})
```
