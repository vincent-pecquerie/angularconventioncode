## Performance : Eviter d’utiliser des fonctions / getters depuis les vues.

* **Règles:** 
    * Les fonctions dans les vues sont appelées à chaque fois que le composent est rendus.
    * Le composant est rendu à chaque évènements (voir section Cycle de vie). 
    * Il faut donc remplacer cet appel par un pipe ou en utilisant un attribut alternatif.
* **Objectifs :**
    * Réduire le nombre d’appel de fonctions inutile.
    * Optimiser le cycle de vie de l’application.

Pour éviter ce problème, on peut recourir à un pipe qui agira que lorsque l’élément cible est modifié ou en utilisant une méthode de calcul manuel.

### Exemple de pipe :

Dans le pipe : 

```typescript
export class FullNamePipe implements PipeTransform {
    transform(person: any, args?: any): any {
        return person.firstName + ' ' + person.lastName;
    }
}
```

Dans la vue :

```html
Bonjour {{ person | Fullname }}
```

### Exemple de méthode:

Dans le contrôleur:

```typescript
setFullName() {
    this.fullname = this.firstname + ' ' + this.lastname ;
} 

onChanges(changes) {
    if(changes.firstname || changes.lastname) {
        this.setFullName();
    }
}
``` 
et dans la vue :  
```html
{{ fullname }}
```
