## Style 02-09 : Convention de Nommages

* **Règles :**
    * Utiliser la notation UpperCamelCase pour nommer les classes de pipes
    * Utiliser la notation lowerCamelCase pour le selecteur.

* **Objectifs:**
    * Garder de la consistance entre les différents pipes et classes.

* **Exemples :**

![Exemple](img/style_02-09_001.png)