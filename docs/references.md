# References

## Documentation
:link: [Documentation Angular](https://angular.io/guide/styleguide#style-guide)

## Checklists

:link: [Checklist de performance](https://github.com/mgechev/angular-performance-checklist)
