## Style 02-04 - Utiliser le nommage de service recommandé

* **Règles:**
    * Une classe de service doit être suffixé de Service. (ex : une clase récupérant des données sera nommée DataService). 
    * Un service lié à un composant doit être préfixé du nom du composant (ex : HeroDataService).
* **Objectifs:**
    * Une convention de nommage permet d’identifier rapidement des services.
        * Cela permet d’identifier rapidement que la classe est un service.

    * Une convention de nommage permet d’avoir de la consistance entre les développements
        * Cela fait gagner du temps aux développeurs en place (automatisme).
        * Cela fait gagner du temps aux nouveaux développeurs (consistante). 

![Exemple](img/style_02-04_001.png)