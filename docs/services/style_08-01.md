## Style 08-01 : Effectuer les appels vers un serveur dans un Service

* **Règles:**
    * Faire les appels XHR dans un service.
    * Faire les appels au local storage dans un service.
* **Objectifs:**
    * Le rôle du contrôleur est de gérer la couche présentation. Séparer la récupération des données permet de respecter la notion de responsabilité unique (Single Responsability de SOLID).
    * Faciliter les tests sur le service et sur le composant en les découplant.
