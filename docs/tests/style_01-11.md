## Style 02-11 : Convention de Nommage des Tests End-To-End (E2E)

* **Règles:**
    * Nommer le fichier de test de la même manière que la fonctionnalité testée.
    * Ajoute le suffixe .e2e-spec.

![Exemple](img/style_02-11_001.png)

* **Objectifs:**
    * Garder de la consistance entre les différentes classes.
    * Identifier rapidement les tests end-to-end

* **Exemples:**

```typescript
import { AppPage } from './app.po';

describe('test-e2-e App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
```