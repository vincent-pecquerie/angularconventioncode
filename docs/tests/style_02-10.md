## Style 02-10 : Convention de Nommage des Tests Unitaires

* **Règles:**
    * Nommer le fichier de test de la même manière que le fichier testé.
    * Ajoute le suffix **.spec**.
* **Objectifs:**
    * Garder de la consistance entre les différentes classes.
    * Identifier rapidement la classe de test associée.

* **Exemples:**

![Exemple](img/style_02-10_001.png)