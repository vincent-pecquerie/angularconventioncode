## Architecture d’un test Unitaire

* **Règles:**
    * Un test doit reposer sur le pattern Arrange, Act et Assert (AAA). 

* **Objectifs:**
    *Distinguer les parties initialisation, opération et test.

Dans Angular, on peut écrire des tests unitaires via le Framework de test Jasmine.
Ces tests sont lancés avec l’orchestrateur de test Karma.

### Références

:link: [Site officiel de Jasmine](https://jasmine.github.io/)
:link: [Site officiel de Karma](https://karma-runner.github.io/latest/index.html)

### Structure d'un fichier Jasmine

```typescript
describe(‘[NOM DU MODULE TESTE]’, () => {

	// Variables Locales ? 
    let fixture : ComponentFixture<AppComponent> ;
    let component: AppComponent;

    // Executé avant chaque test (==> )
    beforeEach(async(() => {
        TestBed.ConfigureTestingModule({	
            declarations : [AppComponent],
            importations : [],
			providers : []
        }).compileComponents();

        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
}));
```

##### Exemple pour un composant

```typescript
// Arrange
const fixture = TestBed.createComponent(AppComponent);
const component = fixture.componentInstance;

// Act
fixture.detectChanges();

// Assert
```

##### Exemple pour un service

```typescript
// Arrange
TestBed.configureTestingModule({ providers: [AppService] });

// Act
it('[AppService] Should be created', inject([CookieService], (service: CookieService) => {
    expect(service).toBeTruthy(); // Assert
}));
```

##### Exemple pour un pipe

```typescript
it(‘[UpperPipe] Should be create’, () => {
	// Arrange
    const pipe = new UpperPipe() ;
    
    // Act
	const input = ‘test’;
    const output = pipe.transform(test);

    // Assert
    expect(output).toEqual(‘TEST’);	
});
```