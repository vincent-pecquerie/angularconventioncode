## TypeScript 01 : Ne pas utiliser any.

* **Règles :**
    * Toujours déclarer et typer les variables et constantes.
    * De ce fait, ne pas utiliser l’instruction any.
* **Objectifs :**
    * Réduire les incohérences avec un typage fort.
    * Empêcher au maximum les conversions implicites problématiques du JavaScript.
