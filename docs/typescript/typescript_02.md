## TypeScript : Utiliser `const` pour déclarer des variables locales qui ne sont pas réassigner

* **Règles :**
    * Si une variable n’est pas ou ne devrait pas être réassigné, utiliser l’instruction const pour la déclarer comme constante.
* **Objectifs :**
    * Eviter les écrasements de valeurs.
